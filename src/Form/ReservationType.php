<?php

namespace App\Form;

use App\Entity\Reservation;
use App\Entity\Room;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                'class' => User::class
            ])
            ->add('room', EntityType::class, [
                'class' => Room::class
            ])
            ->add('dateStart', DateType::class, [
                'format' => 'yyyy-MM-dd',
                'widget' => 'single_text'
            ])
            ->add('dateEnd', DateType::class, [
                'format' => 'yyyy-MM-dd',
                'widget' => 'single_text'
            ])
            ->add('reservationDetails', TextareaType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
            'validation_groups' => ['create', 'update', 'checkin', 'checkout'],
        ]);
    }
}
