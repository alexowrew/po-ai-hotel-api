<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 */
class Role extends \Symfony\Component\Security\Core\Role\Role
{
    const ROLES = [
        'ROLE_USER' => 'Base system user',
        'ROLE_ADMIN' => 'Hotel Admin',
        'ROLE_MANAGER' => 'Hotel Manager',
        'ROLE_GUEST' => 'Hotel Guest',
        'ROLE_LOBBY_CONTROLLER' => 'Hotel Lobby Controller',
        'ROLE_ROBOT_CONTROLLER' => 'Hotel Robot Controller',
        'ROLE_ROOM_CONTROLLER' => 'Hotel Room Controller',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=64)
     * @Serializer\SerializedName("roleLabel")
     */
    private $roleLabel;

    /**
     * Role constructor.
     */
    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getRoleLabel(): ?string
    {
        return $this->roleLabel;
    }

    public function setRoleLabel(string $roleLabel): self
    {
        $this->roleLabel = $roleLabel;

        return $this;
    }
}
