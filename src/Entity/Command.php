<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommandRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class Command
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     */
    private $command;

    /**
     * @ORM\Column(type="json")
     * @Serializer\Type("array")
     * @var \stdClass[]
     * @Serializer\Expose()
     */
    private $data = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Role")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user_role;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Expose()
     */
    private $status = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommand(): ?int
    {
        return $this->command;
    }

    public function setCommand(int $command): self
    {
        $this->command = $command;

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getUserRole(): ?Role
    {
        return $this->user_role;
    }

    public function setUserRole(?Role $user_role): self
    {
        $this->user_role = $user_role;

        return $this;
    }

    public function setStatus($status): self
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus(): bool
    {
        return $this->status;
    }
}
