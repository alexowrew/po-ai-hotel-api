<?php

namespace App\Entity;

use App\Validator\Constraint\UniqueReservation;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReservationRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @UniqueReservation(groups={"create"})
 */
class Reservation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="reservations", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="guid", nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\SerializedName("reservationDetails")
     */
    private $reservationDetails;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Room", inversedBy="reservations", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $room;

    /**
     * @ORM\Column(type="date")
     * @Serializer\SerializedName("dateStart")
     */
    private $dateStart;

    /**
     * @ORM\Column(type="date")
     * @Serializer\SerializedName("dateEnd")
     */
    private $dateEnd;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     * @Serializer\SerializedName("checkInDate")
     */
    private $checkInDate;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     * @Serializer\SerializedName("checkOutDate")
     */
    private $checkOutDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     *
     * @throws \Exception
     */
    public function onAdd()
    {
        $this->setToken(Uuid::uuid4());
    }

    public function getReservationDetails(): ?string
    {
        return $this->reservationDetails;
    }

    public function setReservationDetails(?string $reservationDetails): self
    {
        $this->reservationDetails = $reservationDetails;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getDateStart(): ?\DateTimeInterface
    {
        return $this->dateStart;
    }

    public function setDateStart(\DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCheckInDate()
    {
        return $this->checkInDate;
    }

    /**
     * @param \DateTimeImmutable $checkInDate
     */
    public function setCheckInDate(\DateTimeImmutable $checkInDate): void
    {
        $this->checkInDate = $checkInDate;
    }

    /**
     * @return \DateTime
     */
    public function getCheckOutDate()
    {
        return $this->checkOutDate;
    }

    /**
     * @param \DateTimeImmutable $checkOutDate
     */
    public function setCheckOutDate(\DateTimeImmutable $checkOutDate): void
    {
        $this->checkOutDate = $checkOutDate;
    }
}
