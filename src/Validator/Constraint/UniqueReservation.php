<?php

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueReservation extends Constraint
{
    public $message = 'Room is already occupied';

    public function validatedBy()
    {
        return UniqueReservationValidator::class;
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
