<?php

namespace App\Validator\Constraint;

use App\Entity\Reservation;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class UniqueReservationValidator extends ConstraintValidator
{
    protected $entityManager;

    /**
     * UniqueReservationValidator constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UniqueReservation) {
            throw new UnexpectedTypeException($constraint, UniqueReservation::class);
        }

        if (!$value instanceof Reservation) {
            throw new UnexpectedValueException($value, Reservation::class);
        }

        $params = [
            'roomId' => $value->getRoom()->getId(),
            'dateEnd' => $value->getDateEnd()->format('Y-m-d'),
            'dateStart' => $value->getDateStart()->format('Y-m-d'),
        ];

        $qb = $this->entityManager->createQueryBuilder();
        try {
            $result = $qb
                ->from(Reservation::class, 'r')
                ->select('COUNT(r.id)')
                ->where('r.room = :roomId')
                ->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->between(':dateStart', 'r.dateStart', 'r.dateEnd'),
                        $qb->expr()->between(':dateEnd', 'r.dateStart', 'r.dateEnd')
                    )
                )
                ->setParameters($params)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
            $this->context
                ->buildViolation($e->getMessage())
                ->addViolation();

            return;
        }

        if (intval($result) !== 0) {
            $this->context
                ->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}
