<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ApiController extends AbstractFOSRestController
{
    /** @var SerializerInterface */
    protected $serializer;

    /**
     * ApiController constructor.
     * @param $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    protected function getErrorMessages(ConstraintViolationListInterface $messageList)
    {
        $errors = [];
        foreach ($messageList as $message) {
            $errors[] = $message->getMessage();
        }

        return $errors;
    }
}
