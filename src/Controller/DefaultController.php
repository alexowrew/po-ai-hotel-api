<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends AbstractFOSRestController
{
    /**
     * @Rest\Route("/")
     *
     * @return JsonResponse
     */
    public function indexAction()
    {
        return JsonResponse::create(null, JsonResponse::HTTP_FORBIDDEN);
    }
}
