<?php

namespace App\Controller\v1;

use App\Controller\ApiController;
use App\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends ApiController
{
    /**
     * Gets user
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the user object",
     *     @Model(type=App\Entity\User::class)
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="The user's id"
     * )
     * @SWG\Tag(name="Users")
     *
     * @Rest\Get(path="/user/{id}")
     * @IsGranted("ROLE_USER")
     *
     * @param User $user
     *
     * @return JsonResponse
     */
    public function getUserAction(User $user)
    {
        $res = $this->serializer->serialize($user, 'json');

        return new JsonResponse($res, JsonResponse::HTTP_OK, [], true);
    }
}
