<?php

namespace App\Controller\v1;

use App\Controller\ApiController;
use App\Dto\ReservationDto;
use App\Entity\Reservation;
use App\Entity\User;
use App\Form\ReservationType;
use App\Services\ReservationService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;

class ReservationController extends ApiController
{

    /**
     * @Rest\Get("/reservation/{id}")
     * @SWG\Tag(name="Reservation")
     * @SWG\Response(
     *     response="200",
     *     description="Returns reservation object by ID",
     *     @Model(type=App\Entity\Reservation::class)
     * )
     *
     * @param Reservation $reservation
     * @return JsonResponse
     */
    public function getAction(Reservation $reservation): JsonResponse
    {
        $res = $this->serializer->serialize($reservation, 'json');

        return new JsonResponse($res, JsonResponse::HTTP_OK, [], true);
    }

    /**
     * @Rest\Post("/reservation")
     * @SWG\Tag(name="Reservation")
     * @SWG\Response(
     *     response="204",
     *     description="Reservation created succesfully"
     * )
     * @SWG\Response(
     *     response="400",
     *     description="Incorrect content type or validation failed"
     * )
     * @SWG\Response(
     *     response="500",
     *     description="Error while saving entity"
     * )
     * @SWG\Parameter(
     *     name="data",
     *     in="body",
     *     @Model(type=App\Dto\ReservationDto::class)
     * )
     * @ParamConverter("reservationDto", class="App\Dto\ReservationDto", converter="fos_rest.request_body")
     *
     * @param ReservationDto $reservationDto
     * @param ReservationService $reservationService
     *
     * @return JsonResponse
     */
    public function makeReservationAction(ReservationDto $reservationDto, ReservationService $reservationService): JsonResponse
    {
        try {
            $reservation = $reservationService->prepareReservation($this->getUser(), $reservationDto);
        } catch (\Exception $exception) {
            return new JsonResponse($exception->getMessage(), JsonResponse::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(ReservationType::class, $reservation, [
            'validation_groups' => ['create'],
        ]);

        $form->submit(null, false);

        if (!($form->isSubmitted() && $form->isValid())) {
            return new JsonResponse($this->serializer->serialize($form->getErrors(), 'json'),
                JsonResponse::HTTP_BAD_REQUEST, [], true);
        }

        $repository = $this->getDoctrine()->getRepository(Reservation::class);
        if ($repository->addReservation($reservation)) {
            return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
        }

        return new JsonResponse(null, JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @SWG\Tag(name="Reservation")
     * @Rest\Put("/reservation/{reservation}/check-in")
     *
     * @SWG\Response(
     *     response="204",
     *     description="Check in successful"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Reservation or User not found."
     * )
     *
     * @param Reservation $reservation
     * @param ReservationService $reservationService
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function checkInAction(Reservation $reservation, ReservationService $reservationService): JsonResponse
    {
        if (!$reservationService->checkIn($reservation, $this->getUser())) {
            $errors = $this->serializer->serialize(
                $this->getErrorMessages($reservationService->getLastError()),
                'json'
            );

            return new JsonResponse($errors, JsonResponse::HTTP_BAD_REQUEST, [], true);
        }

        return $this->updateReservation($reservation, ['checkin']);
    }

    /**
     * @SWG\Tag(name="Reservation")
     * @Rest\Put("/reservation/{reservation}/check-out")
     *
     * @SWG\Response(
     *     response="204",
     *     description="Check out successful"
     * )
     *
     * @param Reservation $reservation
     * @param ReservationService $reservationService
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function checkOutAction(Reservation $reservation, ReservationService $reservationService): JsonResponse
    {
        if (!$reservationService->checkOut($reservation, $this->getUser())) {
            $errors = $this->serializer->serialize(
                $this->getErrorMessages($reservationService->getLastError()),
                'json'
            );

            return new JsonResponse($errors, JsonResponse::HTTP_BAD_REQUEST, [], true);
        }

        return $this->updateReservation($reservation);
    }

    /**
     * @param Reservation $reservation
     * @param array $validation_groups
     *
     * @return JsonResponse
     */
    private function updateReservation(Reservation $reservation, array $validation_groups = []): JsonResponse
    {
        $form = $this->createForm(ReservationType::class, $reservation, [
            'validation_groups' => array_merge(['update'], $validation_groups),
        ]);

        $form->submit(null, false);

        if (!($form->isSubmitted() && $form->isValid())) {
            return new JsonResponse($this->serializer->serialize($form->getErrors(), 'json'),
                JsonResponse::HTTP_BAD_REQUEST, [], true);
        }

        $repository = $this->getDoctrine()->getRepository(Reservation::class);
        if ($repository->updateReservation($reservation)) {
            return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
        }

        return new JsonResponse(null, JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @Rest\Get("/reservations")
     * @SWG\Tag(name="Reservation")
     * @SWG\Response(
     *     response="200",
     *     description="Returns array of Reservation objects",
     *     @SWG\Schema(
     *          type="array",
     *          @SWG\Items(ref=@Model(type=Reservation::class))
     *     )
     * )
     *
     * @param ReservationService $reservationService
     * @return JsonResponse
     */
    public function getReservationsByUser(ReservationService $reservationService): JsonResponse
    {
        $reservations = $reservationService->getUserReservations($this->getUser());
        return new JsonResponse($this->serializer->serialize($reservations, 'json'),
            JsonResponse::HTTP_OK, [], true);
    }

    /**
     * @Rest\Delete("/reservation/{reservation}")
     * @SWG\Tag(name="Reservation")
     * @SWG\Response(
     *     response="204",
     *     description="No content is returned on success"
     * )
     *
     * @param Reservation $reservation
     * @param ReservationService $reservationService
     *
     * @return JsonResponse
     */
    public function deleteReservation(Reservation $reservation, ReservationService $reservationService): JsonResponse
    {
        if (!($this->isGranted('ROLE_ADMIN') || $this->isGranted('ROLE_GUEST'))) {
            return new JsonResponse([], JsonResponse::HTTP_FORBIDDEN);
        }

        /** @var User $user */
        $user = $this->getUser();
        if (in_array('ROLE_GUEST', $user->getRoles()) && $reservation->getUser()->getId() !== $user->getId()) {
            return new JsonResponse([], JsonResponse::HTTP_FORBIDDEN);
        }

        if ($reservationService->deleteReservation($reservation)) {
            return new JsonResponse([], JsonResponse::HTTP_NO_CONTENT);
        }

        return new JsonResponse([], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
    }
}
