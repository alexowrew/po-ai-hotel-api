<?php

namespace App\Controller\v1;

use App\Controller\ApiController;
use App\Services\RoomService;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RoomController extends ApiController
{
    /**
     * @Rest\Get("/rooms")
     * @SWG\Tag(name="Rooms")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returns array of Room objects",
     *     @SWG\Schema(
     *          type="array",
     *          @SWG\Items(ref=@Model(type=App\Entity\Room::class))
     *     )
     * )
     *
     * @IsGranted("ROLE_ROOM_CONTROLLER")
     *
     * @param RoomService $roomService
     * @return JsonResponse
     */
    public function getRoomSetup(RoomService $roomService)
    {
        $roomSetup = $roomService->getRoomSetup();

        $context = SerializationContext::create();
        $context->setGroups(['setup']);
        $setup = $this->serializer->serialize($roomSetup, 'json', $context);

        return new JsonResponse($setup, JsonResponse::HTTP_OK, [], true);
    }

    /**
     * @Rest\Get("/rooms/available")
     * @SWG\Tag(name="Rooms")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returns array of available Room objects",
     *     @SWG\Schema(
     *          type="array",
     *          @SWG\Items(ref=@Model(type=App\Entity\Room::class))
     *     )
     * )
     *
     * @param Request $request
     * @param RoomService $roomService
     * @return JsonResponse
     */
    public function getAvailableRoomsAction(Request $request, RoomService $roomService)
    {
        $data = $roomService->getAvailableRooms();

        return new JsonResponse($this->serializer->serialize($data, 'json'), JsonResponse::HTTP_OK, [], true);
    }

}
