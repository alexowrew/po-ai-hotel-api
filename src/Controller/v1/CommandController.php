<?php

namespace App\Controller\v1;

use App\Controller\ApiController;
use App\Entity\Command;
use App\Services\CommandService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CommandController extends ApiController
{
    /**
     * @Rest\Get("/commands")
     * @SWG\Tag(name="Commands")
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     type="integer",
     *     description="Command limit to be returned",
     *     default="5"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returns array of Command objects",
     *     @SWG\Schema(
     *          type="array",
     *          @SWG\Items(ref=@Model(type=App\Entity\Command::class))
     *     )
     * )
     *
     * @param CommandService $commandService
     * @param Request $request
     * @return JsonResponse
     */
    public function index(CommandService $commandService, Request $request)
    {
        $limit = $request->get('limit') ?? 5;
        $commands = $commandService->getCommandsForUserRoles($this->getUser(), $limit);
        $data = $this->serializer->serialize($commands, 'json');

        return new JsonResponse($data, JsonResponse::HTTP_OK, [], true);
    }

    /**
     * @Rest\Put("/command/{command}/finish")
     * @SWG\Tag(name="Commands")
     *
     * @SWG\Response(
     *     response="204",
     *     description="Success"
     * )
     * @SWG\Response(
     *     response="500",
     *     description="Error while persisting entity"
     * )
     *
     * @param Command $command
     * @param CommandService $commandService
     *
     * @return JsonResponse
     */
    public function finishCommand(Command $command, CommandService $commandService)
    {
        if ($commandService->evaluateCommand($command) && $commandService->markCommandAsDone($command)) {
            return new JsonResponse([], JsonResponse::HTTP_NO_CONTENT);
        }

        return new JsonResponse([], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
    }
}
