<?php

namespace App\Repository;

use App\Entity\Room;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Room|null find($id, $lockMode = null, $lockVersion = null)
 * @method Room|null findOneBy(array $criteria, array $orderBy = null)
 * @method Room[]    findAll()
 * @method Room[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Room::class);
    }

    /**
     * @param null $criteria
     * @param int|null $maxResults
     * @param int|null $firstResult
     * @return Room[]
     */
    public function findAvailableRooms($criteria = null, ?int $maxResults = 20, ?int $firstResult = 0)
    {
        $qb = $this->createQueryBuilder('room')
            ->leftJoin('room.reservations', 'reservations');

        $qb->where(
            $qb->expr()->isNull('reservations.room')
        );

        if (!empty($criteria)) {
            $qb->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->gte('room.floor', $criteria['floor'])
                )
            );

            $qb->leftJoin('room.equipment', 'equipment')
                ->andWhere(
                    $qb->expr()->eq('equipment.type', 1)
                )
                ->groupBy('room.id')
                ->having(
                    $qb->expr()->eq($qb->expr()->count('equipment'), $criteria['peopleNo'])
                );
        }

        return $qb->setMaxResults($maxResults)
            ->setFirstResult($firstResult)
            ->getQuery()
            ->getResult();
    }
}
