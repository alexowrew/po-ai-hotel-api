<?php

namespace App\Services;

use App\Entity\Command;
use App\Entity\Equipment;
use App\Interfaces\CommandExecutionInterface;
use Doctrine\ORM\EntityManagerInterface;

class EquipmentService implements CommandExecutionInterface
{
    protected $entityManager;

    const STATUS_FULL = 100;

    /**
     * DeviceService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function executeCommand(Command $command): bool
    {
        $data = $command->getData();

        $entity = $this->entityManager->getRepository(Equipment::class)
            ->findOneBy([
                'id' => $data['subject']['id'],
            ]);

        return $this->doCommand($entity, $command);
    }

    private function doCommand(Equipment $entity, Command $command): bool
    {
        switch ($command->getCommand()) {
            case CommandService::COMMAND_FIX_EQUIPMENT:
                $entity->setStatus(self::STATUS_FULL);
                break;
        }

        $this->entityManager->flush();

        return true;
    }
}
