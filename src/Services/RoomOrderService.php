<?php

namespace App\Services;

use App\Entity\Command;
use App\Interfaces\CommandExecutionInterface;
use Doctrine\ORM\EntityManagerInterface;

class RoomOrderService implements CommandExecutionInterface
{
    protected $entityManager;

    /**
     * DeviceService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function executeCommand(Command $command): bool
    {
        return $this->doCommand();
    }

    private function doCommand(): bool
    {
        // CommandService::COMMAND_PREPARE_FOOD
        // deliver to room
        return true;
    }
}
