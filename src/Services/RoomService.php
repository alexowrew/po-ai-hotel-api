<?php

namespace App\Services;

use App\Entity\Command;
use App\Entity\Room;
use App\Interfaces\CommandExecutionInterface;
use App\Utils\Timespan;
use Doctrine\ORM\EntityManagerInterface;

class RoomService implements CommandExecutionInterface
{
    const STATUS_FULL = 100;
    protected $entityManager;

    /**
     * RoomService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getAvailableRooms(Timespan $timespan = null)
    {
        return $this->entityManager->getRepository(Room::class)
            ->findAvailableRooms();
    }

    public function getRoomSetup()
    {
        return $this->entityManager->getRepository(Room::class)->findAll();
    }

    public function executeCommand(Command $command): bool
    {
        $data = $command->getData();

        $entity = $this->entityManager->getRepository(Room::class)
            ->findOneBy([
                'id' => $data['subject']['id'],
            ]);

        return $this->doCommand($entity, $command);
    }

    private function doCommand(Room $entity, Command $command): bool
    {
        switch ($command->getCommand()) {
            case CommandService::COMMAND_MAKEUP_ROOM:
                $entity->setStatus(self::STATUS_FULL);
                break;
        }

        $this->entityManager->flush();

        return true;
    }
}
