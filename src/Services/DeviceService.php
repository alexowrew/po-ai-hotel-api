<?php

namespace App\Services;

use App\Entity\Command;
use App\Entity\Device;
use App\Interfaces\CommandExecutionInterface;
use Doctrine\ORM\EntityManagerInterface;

class DeviceService implements CommandExecutionInterface
{
    protected $entityManager;

    const STATUS_FULL = 100;

    /**
     * DeviceService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function executeCommand(Command $command): bool
    {
        $data = $command->getData();

        $entity = $this->entityManager->getRepository(Device::class)
            ->findOneBy([
                'id' => $data['subject']['id'],
            ]);

        return $this->doCommand($entity, $command);
    }

    private function doCommand(Device $entity, Command $command): bool
    {
        switch ($command->getCommand()) {
            case CommandService::COMMAND_FIX_DEVICE:
                $entity->setStatus(self::STATUS_FULL);
                break;
        }

        $this->entityManager->flush();

        return true;
    }
}
