<?php

namespace App\Services;

use App\Assembler\ReservationAssembler;
use App\Dto\ReservationDto;
use App\Entity\Reservation;
use App\Entity\Room;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraints\IdenticalTo;
use Symfony\Component\Validator\Constraints\IsNull;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ReservationService
{
    protected $validator;
    protected $lastViolations;
    protected $entityManager;

    /**
     * ReservationService constructor.
     * @param ValidatorInterface $validator
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(ValidatorInterface $validator, EntityManagerInterface $entityManager)
    {
        $this->validator = $validator;
        $this->entityManager = $entityManager;

        $this->lastViolations = new ConstraintViolationList();
    }

    /**
     * @param Reservation $reservation
     * @param User $user
     *
     * @return ConstraintViolationListInterface
     * @throws \Exception
     */
    public function canCheckIn(Reservation $reservation, User $user): ConstraintViolationListInterface
    {
        $rangeConstraint = new Range([
            'min' => $reservation->getDateStart(),
            'max' => $reservation->getDateEnd(),
        ]);

        $userConstraint = new IdenticalTo([
            'value' => $user->getId(),
            'message' => 'Invalid user selected.',
        ]);

        $checkinConstraint = new IsNull([
            'message' => 'User is already checked in'
        ]);

        $this->lastViolations->addAll($this->validator->validate(\DateTimeImmutable::createFromFormat('Y-m-d|', date('Y-m-d')), $rangeConstraint));
        $this->lastViolations->addAll($this->validator->validate($reservation->getUser()->getId(), $userConstraint));
        $this->lastViolations->addAll($this->validator->validate($reservation->getCheckInDate(), $checkinConstraint));

        return $this->lastViolations;
    }

    /**
     * @param Reservation $reservation
     * @param User $user
     *
     * @return ConstraintViolationListInterface
     */
    public function canCheckOut(Reservation $reservation, User $user): ConstraintViolationListInterface
    {
        $userConstraint = new IdenticalTo([
            'value' => $user->getId(),
            'message' => 'Invalid user selected.',
        ]);

        $checkinConstraint = new NotNull([
            'message' => 'User is not checked in'
        ]);

        $checkoutConstraint = new IsNull([
            'message' => 'User is already checked out'
        ]);

        // @TODO check if bill is paid?

        $this->lastViolations->addAll($this->validator->validate($reservation->getUser()->getId(), $userConstraint));
        $this->lastViolations->addAll($this->validator->validate($reservation->getCheckInDate(), $checkinConstraint));
        $this->lastViolations->addAll($this->validator->validate($reservation->getCheckOutDate(), $checkoutConstraint));

        return $this->lastViolations;
    }

    /**
     * @param Reservation $reservation
     * @param User $user
     *
     * @return bool
     * @throws \Exception
     */
    public function checkIn(Reservation $reservation, User $user)
    {
        $violations = $this->canCheckIn($reservation, $user);

        if ($violations->count() > 0) {
            return false;
        }

        $reservation->setCheckInDate(new \DateTimeImmutable());

        return true;
    }

    /**
     * @param Reservation $reservation
     * @param User $user
     *
     * @return bool
     * @throws \Exception
     */
    public function checkOut(Reservation $reservation, User $user)
    {
        $violations = $this->canCheckOut($reservation, $user);

        if ($violations->count() > 0) {
            return false;
        }

        $reservation->setCheckOutDate(new \DateTimeImmutable());

        return true;
    }

    public function getLastError(): ConstraintViolationListInterface
    {
        return $this->lastViolations;
    }

    public function prepareReservation(User $user, ReservationDto $reservationDto): Reservation
    {
        $reservation = ReservationAssembler::fromDto($reservationDto);

        $reservation->setUser($user);

        $roomRepository = $this->entityManager->getRepository(Room::class);

        $rooms = $roomRepository->findAvailableRooms([
            'floor' => $reservationDto->floor,
            'peopleNo' => $reservationDto->bedNo,
        ]);

        if (empty($rooms)) {
            throw new \Exception('@TODO no rooms found exception');
        }

        $room = reset($rooms);

        return $reservation->setRoom($room);
    }

    public function getUserReservations(User $user)
    {
        if (in_array('ROLE_ADMIN', $user->getRoles())) {
            return $this->entityManager->getRepository(Reservation::class)->findAll();
        }

        return $this->entityManager->getRepository(Reservation::class)->findBy([
            'user' => $user
        ], [
            'dateStart' => 'ASC'
        ]);
    }

    public function deleteReservation(Reservation $reservation)
    {
        $this->entityManager->remove($reservation);
        $this->entityManager->flush();

        return true;
    }
}
