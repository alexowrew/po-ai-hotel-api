<?php

namespace App\Services;

use App\Entity\Command;
use App\Entity\User;
use App\Interfaces\CommandExecutionInterface;
use Doctrine\ORM\EntityManagerInterface;

class CommandService
{
    const COMMAND_FIX_DEVICE = 201;
    const COMMAND_FIX_EQUIPMENT = 202;
    const COMMAND_MAKEUP_ROOM = 401;
    const COMMAND_PREPARE_FOOD = 501;

    protected $entityManager;

    protected $deviceService;
    protected $roomOrderService;
    protected $equipmentService;
    protected $roomService;


    /**
     * CommandService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getCommandsForUserRoles(User $user, int $limit = 5)
    {
        return $this->entityManager->getRepository(Command::class)
            ->findBy([
                'status' => false,
                'user_role' => $user->getRoles(),
            ], null, $limit);
    }

    public function markCommandAsDone(Command $command)
    {
        $command->setStatus(true);
        if ($this->entityManager->getRepository(Command::class)->update($command)) {
            return true;
        }

        return false;
    }

    public function evaluateCommand(Command $command)
    {
        try {
            return $this->getServiceClass($command->getCommand())
                ->executeCommand($command);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $command
     * @return CommandExecutionInterface
     * @throws \Exception
     */
    private function getServiceClass($command): CommandExecutionInterface
    {
        switch ($command) {
            case self::COMMAND_FIX_DEVICE:
                return $this->getDeviceService();

            case self::COMMAND_FIX_EQUIPMENT:
                return $this->getEquipmentService();

            case self::COMMAND_PREPARE_FOOD:
                return $this->getRoomOrderService();

            case self::COMMAND_MAKEUP_ROOM:
                return $this->getRoomService();
        }

        throw new \Exception('Command not found.');
    }

    public function getDeviceService(): DeviceService
    {
        return $this->deviceService;
    }

    public function setDeviceService(DeviceService $deviceService): void
    {
        $this->deviceService = $deviceService;
    }

    public function getRoomOrderService(): RoomOrderService
    {
        return $this->roomOrderService;
    }

    public function setRoomOrderService(RoomOrderService $roomOrderService): void
    {
        $this->roomOrderService = $roomOrderService;
    }

    public function getEquipmentService(): EquipmentService
    {
        return $this->equipmentService;
    }

    public function setEquipmentService(EquipmentService $equipmentService): void
    {
        $this->equipmentService = $equipmentService;
    }

    public function getRoomService(): RoomService
    {
        return $this->roomService;
    }

    public function setRoomService(RoomService $roomService): void
    {
        $this->roomService = $roomService;
    }
}
