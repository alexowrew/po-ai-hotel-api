<?php

namespace App\Utils;

class Timespan
{
    /** @var \DateTime */
    protected $from;
    /** @var \DateTime */
    protected $to;

    /**
     * Timespan constructor.
     * @param \DateTime $from
     * @param \DateTime $to
     */
    public function __construct(\DateTime $from, \DateTime $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    public function from(): \DateTime
    {
        return $this->from;
    }

    public function to(): \DateTime
    {
        return $this->to;
    }

}
