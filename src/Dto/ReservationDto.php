<?php

namespace App\Dto;

use JMS\Serializer\Annotation as Serializer;

class ReservationDto
{
    /**
     * @Serializer\Type("integer")
     * @var integer
     */
    public $floor;

    /**
     * @Serializer\Type("integer")
     * @Serializer\SerializedName("bedNo")
     * @var integer
     */
    public $bedNo;

    /**
     * @Serializer\Type("DateTime<'','','Y-m-d\TH:i:s.uT'>")
     * @Serializer\SerializedName("dateStart")
     * @var \DateTime
     */
    public $dateStart;

    /**
     * @Serializer\Type("DateTime<'','','Y-m-d\TH:i:s.uT'>")
     * @Serializer\SerializedName("dateEnd")
     * @var \DateTime
     */
    public $dateEnd;

    /**
     * @Serializer\Type("string")
     * @var string
     */
    public $standard;

    /**
     * @Serializer\Type("string")
     * @Serializer\SerializedName("reservationDetails")
     * @var string
     */
    public $reservationDetails;
}
