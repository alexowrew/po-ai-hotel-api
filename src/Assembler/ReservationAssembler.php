<?php

namespace App\Assembler;

use App\Dto\ReservationDto;
use App\Entity\Reservation;

final class ReservationAssembler
{

    public static function fromDto(ReservationDto $reservationDto, ?Reservation $reservation = null): Reservation
    {
        if ($reservation === null) {
            $reservation = new Reservation();
        }

        $reservation->setDateStart($reservationDto->dateStart);
        $reservation->setDateEnd($reservationDto->dateEnd);
        $reservation->setReservationDetails($reservationDto->reservationDetails);

        return $reservation;
    }

    public static function toDto(): ReservationDto
    {
        return new ReservationDto();
    }
}
