<?php

namespace App\Interfaces;

use App\Entity\Command;

interface CommandExecutionInterface
{
    public function executeCommand(Command $command): bool;
}
