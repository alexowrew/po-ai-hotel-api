<?php

namespace App\DataFixtures;

use App\Entity\Device;
use App\Entity\Equipment;
use App\Entity\Room;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RoomFixtures extends Fixture
{
    const HOTEL_FLOORS = 10;
    const HOTEL_ROOMS_PER_FLOOR = 20;

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < self::HOTEL_FLOORS; $i++) {
            for ($j = 1; $j <= self::HOTEL_ROOMS_PER_FLOOR; $j++) {
                $room = new Room();
                $room->setFloor($i)
                    ->setNumber($j);

                $equipment = $this->createEquipment();
                array_walk($equipment, function ($equipment) use ($room, $manager) {
                    $manager->persist($equipment);

                    $room->addEquipment($equipment);
                });

                $devices = $this->createDevice();
                array_walk($devices, function ($device) use ($room, $manager) {
                    $manager->persist($device);

                    $room->addDevice($device);
                });

                $manager->persist($room);
            }
        }

        $manager->flush();
    }

    private function createEquipment()
    {
        $bedCount = rand(1, 5);
        $equipmentCount = rand(0, 3);
        $allEquipment = [];
        for ($i = 0; $i <= $bedCount; $i++) {
            $equipment = new Equipment();
            $equipment->setType(1)
                ->setName('Bed')
                ->setStatus(100);

            $allEquipment[] = $equipment;
        }

        for ($i = 0; $i <= $equipmentCount; $i++) {
            $equipment = new Equipment();
            $type = rand(2, 4);
            $equipment->setType($type)
                ->setName('Equipment type #' . $type)
                ->setStatus(rand(50, 100));

            $allEquipment[] = $equipment;
        }

        return $allEquipment;
    }

    private function createDevice()
    {
        $deviceCount = rand(1, 4);
        $allDevices = [];

        for ($i = 0; $i <= $deviceCount; $i++) {
            $device = new Device();
            $type = rand(1, 3);
            $device->setType($type)
                ->setName('Device type #' . $type)
                ->setStatus(rand(30, 100));

            $allDevices[] = $device;
        }

        return $allDevices;
    }
}
