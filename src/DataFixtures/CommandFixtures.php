<?php

namespace App\DataFixtures;

use App\Entity\Command;
use App\Entity\Role;
use App\Entity\Room;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use JMS\Serializer\SerializerInterface;

class CommandFixtures extends Fixture implements DependentFixtureInterface
{
    const COMMAND_FIX_DEVICE = 201;
    const COMMAND_FIX_EQUIPMENT = 202;
    const COMMAND_MAKEUP_ROOM = 401;

    const COMMAND_PREPARE_FOOD = 501;

    protected $serializer;

    /**
     * CommandFixtures constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }


    public function load(ObjectManager $manager)
    {
        $rooms = $manager->getRepository(Room::class)->findAll();
        $role = $manager->getRepository(Role::class)->findBy([
            'role' => 'ROLE_ROBOT_CONTROLLER',
        ]);

        foreach ($rooms as $room) {
            $devices = $room->getDevices();
            foreach ($devices as $device) {
                if ($device->getStatus() < 100) {
                    $command = new Command();

                    $command->setUserRole($role[0])
                        ->setData([
                            'subject' => $this->serializer->deserialize($this->serializer->serialize($device, 'json'), 'array', 'json')
                        ])
                        ->setCommand(self::COMMAND_FIX_DEVICE);

                    $manager->persist($command);
                }
            }

            $equipments = $room->getEquipment();
            foreach ($equipments as $equipment) {
                if ($equipment->getStatus() < 100) {
                    $command = new Command();

                    $command->setUserRole($role[0])
                        ->setData([
                            'subject' => $this->serializer->deserialize($this->serializer->serialize($equipment, 'json'), 'array', 'json')
                        ])
                        ->setCommand(self::COMMAND_FIX_EQUIPMENT);

                    $manager->persist($command);
                }
            }

            if (rand(0, 99) > 33) {
                $command = new Command();

                $command->setUserRole($role[0])
                    ->setData([
                        'subject' => $this->serializer->deserialize($this->serializer->serialize($room, 'json'), 'array', 'json')
                    ])
                    ->setCommand(self::COMMAND_MAKEUP_ROOM);

                $manager->persist($command);
            }


            if (rand(0, 99) > 33) {
                $command = new Command();

                $command->setUserRole($role[0])
                    ->setData([
                        'subject' => $this->serializer->deserialize($this->serializer->serialize($room, 'json'), 'array', 'json')
                    ])
                    ->setCommand(self::COMMAND_PREPARE_FOOD);

                $manager->persist($command);
            }
        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            RoomFixtures::class,
            RoleFixtures::class
        ];
    }
}
