<?php

namespace App\DataFixtures;

use App\Entity\Role;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    protected $passwordEncoder;

    private const USERS = [
        ['admin@ai-hotel', 'pass', ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_MANAGER']],
        ['manager@ai-hotel', 'pass', ['ROLE_USER', 'ROLE_MANAGER']],
        ['lobby@ai-hotel', 'lobby', ['ROLE_USER', 'ROLE_LOBBY_CONTROLLER']],
        ['robot@ai-hotel', 'robot', ['ROLE_USER', 'ROLE_ROBOT_CONTROLLER']],
        ['room@ai-hotel', 'room', ['ROLE_USER', 'ROLE_ROOM_CONTROLLER']],
        ['guest@ai-hotel', 'room', ['ROLE_USER', 'ROLE_GUEST']],
    ];

    /**
     * UserFixtures constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        foreach (self::USERS as $userData) {
            $user = new User();

            $user->setEmail($userData[0])
                ->setPassword($this->passwordEncoder->encodePassword($user, $userData[1]));

            foreach ($userData[2] as $role) {
                /** @var Role $roleObject */
                $roleObject = $this->getReference(RoleFixtures::itemRefName($role));
                $user->addRole($roleObject);
            }

            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [RoleFixtures::class];
    }
}
