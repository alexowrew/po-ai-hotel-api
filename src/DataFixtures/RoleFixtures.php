<?php

namespace App\DataFixtures;

use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RoleFixtures extends Fixture
{
    const ROLE_REFERENCE = 'role-ref';

    public function load(ObjectManager $manager)
    {
        foreach (Role::ROLES as $role => $label) {
            $roleObject = new Role();
            $roleObject->setRoleLabel($label)
                ->setRole($role);

            $this->setReference(self::itemRefName($role), $roleObject);

            $manager->persist($roleObject);
        }


        $manager->flush();
    }

    public static function itemRefName($itemName)
    {
        return self::ROLE_REFERENCE . '-' . $itemName;
    }

}
